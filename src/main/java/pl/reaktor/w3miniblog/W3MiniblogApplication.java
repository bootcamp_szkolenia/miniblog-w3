package pl.reaktor.w3miniblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class W3MiniblogApplication {

	public static void main(String[] args) {
		SpringApplication.run(W3MiniblogApplication.class, args);
	}
}
