package pl.reaktor.w3miniblog.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter @Setter
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date added;
    @Column(length = 2000)
    private String commentBody;

    @ManyToOne
    private Post post;
}
