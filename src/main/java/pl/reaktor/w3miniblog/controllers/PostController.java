package pl.reaktor.w3miniblog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.reaktor.w3miniblog.entities.Comment;
import pl.reaktor.w3miniblog.entities.Post;
import pl.reaktor.w3miniblog.repositories.CommentRepository;
import pl.reaktor.w3miniblog.repositories.PostRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class PostController {

    private PostRepository postRepository;
    private CommentRepository commentRepository;

    @Autowired
    public PostController(PostRepository postRepository,
                          CommentRepository commentRepository) {
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
    }

    @RequestMapping(value = "/post/add", method = RequestMethod.GET)
    public String addPostForm(){
        System.out.println("addPostForm");
        return "post/postAdd";
    }

    @RequestMapping(value = "/post/add", method = RequestMethod.POST)
    public String addPostAction(@RequestParam String title,
                                @RequestParam String content){
        System.out.println("addPostAction: " + title + ", " + content);

//        Post post = new Post();
//        post.setTitle(title);
//        post.setContent(content);
//        post.setAdded(new Date());

        Post post = Post.builder()
                .added(new Date())
                .content(content)
                .title(title)
                .build();

        Post savedPost = postRepository.save(post);

        System.out.println(post);
        System.out.println(savedPost);
        System.out.println("Post saved: " + post.getId() + " / " + savedPost.getId());

        return "post/postAdded";
    }

    //@RequestMapping(value = "/posts", method = RequestMethod.GET)
    @GetMapping("/posts")
    public String showAllPosts(Model model){
        List<Post> posts = postRepository.findAll();
        model.addAttribute("postsModel", posts);
        return "post/posts";
    }

    @GetMapping("/post/{postId}")
    public String showPost(@PathVariable (name = "postId") Long postIdParam,
                           Model model){
        Optional<Post> postOptional = postRepository.findById(postIdParam);
        if(postOptional.isPresent()){
            model.addAttribute("post", postOptional.get());

//            List<Comment> comments = commentRepository.findAll();
//            List<Comment> goodComments = new ArrayList<>();
//            for (Comment comment : comments) {
//                if(comment.getPost().getId().equals(postIdParam)){
//                    goodComments.add(comment);
//                }
//            }

//            List<Comment> goodComments = commentRepository.findAll().stream()
//                    .filter(c -> c.getPost().getId().equals(postIdParam))
//                    .collect(Collectors.toList());

            List<Comment> goodComments = commentRepository.findAllByPostId(postIdParam);


            model.addAttribute("comments", goodComments);

            return "post/post";
        }

        return "post/postNotFound";
    }

    @GetMapping("/search")
    public String searchPosts(@RequestParam String q, Model model,
                              @RequestParam String searchBy){

        List<Post> posts;

        if("title".equals(searchBy)){
            posts = postRepository.findAllByTitleContains(q);
        } else if("titleOrContent".equals(searchBy)){
            posts = postRepository.findAllByTitleContainsOrContentContains(q, q);
        } else if("titleAndContent".equals(searchBy)){
            posts = postRepository.findAllByTitleContainsAndContentContains(q, q);
        } else {
            posts = new ArrayList<>();
        }

        model.addAttribute("phrase", q);
        model.addAttribute("posts", posts);
        return "post/searchResults";
    }

    @GetMapping("/search2")
    public String searchPosts2(@RequestParam String q, Model model){

        List<Post> posts = postRepository.findPostsWithPhrase(q);

        model.addAttribute("phrase", q);
        model.addAttribute("posts", posts);
        return "post/searchResults";
    }
    @PostMapping("/post/{postId}/comment/add")
    public String addCommentAction(@RequestParam String commentBody,
                                   @PathVariable Long postId,
                                   @RequestParam (required = false) Long postHiddenId){

        Optional<Post> postOptional = postRepository.findById(postId);

        if(!postOptional.isPresent()){
            return "redirect:/posts";
        }

        Comment comment = new Comment();
        comment.setCommentBody(commentBody);
        comment.setAdded(new Date());
        comment.setPost(postOptional.get());

//        Tak nie robimy!
//        Post post = new Post(); post.setId(postId);
//        comment.setPost(post);

        commentRepository.save(comment);

        return "redirect:/post/" + postId;
    }
}
