package pl.reaktor.w3miniblog.forms;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Setter @Getter
@ToString(exclude = {"pass", "passRepeat"})
public class RegisterForm {

    @NotEmpty
    private String email;

    private String pass;

    private String passRepeat;

    @NotBlank
    private String firstName;

    @Size(min = 5, max = 10, message = "Nazwisko musi mieć od {min} do {max} znaków.")
    private String lastName;

}
