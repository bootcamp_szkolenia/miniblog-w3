package pl.reaktor.w3miniblog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.reaktor.w3miniblog.entities.Post;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    List<Post> findAllByTitleContains(String title);
    List<Post> findAllByTitleContainsOrContentContains(String title, String content);
    List<Post> findAllByTitleContainsAndContentContains(String title, String content);
    List<Post> findAllByTitleContainsOrContentContainsOrId(String title, String content, Long id);
    List<Post> findAllByTitleContainsOrContentContainsOrderByTitleDesc(String title, String content);

    @Query("SELECT p FROM Post p WHERE p.title like %:phrase% AND p.content like %:phrase%")
    List<Post> findPostsWithPhrase(@Param("phrase") String phrase/*, @Param("phrase2") String phrase2*/);

}
