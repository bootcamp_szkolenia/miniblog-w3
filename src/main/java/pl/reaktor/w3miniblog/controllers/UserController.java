package pl.reaktor.w3miniblog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.reaktor.w3miniblog.entities.Role;
import pl.reaktor.w3miniblog.entities.User;
import pl.reaktor.w3miniblog.forms.RegisterForm;
import pl.reaktor.w3miniblog.repositories.RoleRepository;
import pl.reaktor.w3miniblog.repositories.UserRepository;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
public class UserController {

    private UserRepository userRepository;
    private RoleRepository roleRepository;

    @Autowired
    public UserController(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @GetMapping("/user/register")
    public String registerForm(Model model){
        model.addAttribute("registerForm", new RegisterForm());
        return "register";
    }

    //@RequestMapping(name = "/user/register", method = RequestMethod.POST)
    @PostMapping("/user/register")
    public String registerFormAction(
            @ModelAttribute @Valid RegisterForm registerForm,
            BindingResult bindingResult,
            Model model){

        if(registerForm.getPass() == null
                || !registerForm.getPass().equals(registerForm.getPassRepeat())){
            bindingResult.rejectValue("pass", "123",
                    "Hasła muszą być takie same");
            bindingResult.rejectValue("passRepeat", "123",
                    "Hasła muszą być takie same");
        }
//        if(!registerForm.getEmail().contains("@")){
//            model.addAttribute("emailError", "Błędny email");
//        }
        System.out.println(registerForm);
        System.out.println(bindingResult.toString());
        System.out.println(registerForm.getPass());

        List<User> allByEmail = userRepository.findAllByEmail(registerForm.getEmail());
        if(!allByEmail.isEmpty()){
            bindingResult.rejectValue("email", "123",
                    "Dany email już istnieje.");
        }

        if(bindingResult.hasErrors()){
            return "register";
        }


        BCryptPasswordEncoder bCryptPasswordEncoder =
                new BCryptPasswordEncoder();

        User user = new User();
        user.setEmail(registerForm.getEmail());
        user.setFirstName(registerForm.getFirstName());
        user.setLastName(registerForm.getLastName());
        user.setAdded(new Date());
        user.setActive(Boolean.TRUE);
//        Optional<Role> optionalRole = roleRepository.findOneByRoleName("USER");
//        Role role = optionalRole.get();
//        user.getRoles().add(role);
        roleRepository.findOneByRoleName("USER")
                .ifPresent(role -> user.getRoles().add(role));
        user.setPassword(bCryptPasswordEncoder.encode(registerForm.getPass()));

        userRepository.save(user);
        //return "goodJob";
        return "redirect:/";
    }

    @GetMapping("/user/login")
    public String loginForm(){

        return "login";
    }
}
