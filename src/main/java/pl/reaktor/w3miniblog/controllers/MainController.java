package pl.reaktor.w3miniblog.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {

    @RequestMapping("/")
    public String indexPage(){
        return "homePage";
    }

    @RequestMapping("/hello")
    public String helloPage(
            @RequestParam(name = "firstName", required = false)
                    String firstNameParam,
            @RequestHeader("User-Agent") String userAgent
            //,Model model
    ) {
//        model.addAttribute("value", "Value #1");
//        model.addAttribute("ua", userAgent);
//
//        if (firstNameParam != null){
//            model.addAttribute("firstName", firstNameParam);
//        }
        return "helloPage";
    }

    @RequestMapping("/name/{name}")
    public String showName(Model model,
                           @PathVariable String name){

        model.addAttribute("modelName", name);

        return "showName";
    }


    //https://ksiegarnia.pwn.pl/Chromatografia-i-techniki-elektromigracyjne,487064082,p.html
    @RequestMapping("/{title},{id},p.html")
    public String showBook(Model model, @PathVariable String title,
                           @PathVariable String id){
        model.addAttribute("bookId", id);
        model.addAttribute("bookName", title);
        return "showBook";
    }
}
